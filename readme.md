# What

This monitors dbus for notifications and forwards them to a matrix room.

# Usage
Do

```
cargo build
```

then create a `config.json` file like:

```
{
	"homeserver": "https://matrix-client.org",
	"name": "your username",
	"password": "password1234",
	"room_id": "!jT88C4d3WCqeaYh3:matrix-client.org"
}
```

Note that the room id is not the alias/name of the room but an internal random id.

Run
```
./target/debug/dbusmatrix config.json
```

# Systemd

Here's an example unit file:

```
[Unit]
Description=dbus-matrix

[Service]
Type=simple
Restart=always
RestartSec=60s
ExecStart=/path/to/dbusmatrix /path/to/your/config.json

[Install]
WantedBy=default.target
```

To install it as a user, modify the paths and put this in `~/.config/systemd/user/dbusmatrix.service` and run

```
systemctl enable --user dbusmatrix
systemctl start --user dbusmatrix
```

Also make sure your DBUS env is in systemd after you log in:
```
systemctl --user import-environment
```

dbusmatrix will restart until the environment var is present (otherwise it does something useless with no error messages).