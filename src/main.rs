use std::collections::HashMap;
use std::env;
use std::fs::File;
use std::path::PathBuf;
use std::str::FromStr;
use std::thread::sleep;
use std::time::Duration;

use futures_util::TryStreamExt;
use matrix_sdk::events::{room::message::MessageEventContent, AnyMessageEventContent};
use matrix_sdk::reqwest::Url;
use matrix_sdk::Client;
use ruma::RoomId;
use serde::Deserialize;
use structopt::StructOpt;
use tokio::runtime::Builder;
use tokio::task::LocalSet;
use zbus::export::zvariant::Value;

#[derive(Debug, StructOpt)]
#[structopt()]
struct Opt {
    /// Path to json config file
    config: PathBuf,
}

#[derive(Deserialize)]
struct Config {
    /// Server address, like server.com:443
    homeserver: String,
    name: String,
    password: String,
    /// Room id, like !1234:server.com
    room_id: String,
}

type Notification<'a> = (
    &'a str,
    u32,
    &'a str,
    &'a str,
    &'a str,
    Vec<&'a str>,
    HashMap<&'a str, Value<'a>>,
    i32,
);

fn main() -> Result<(), String> {
    env::var("DBUS_SESSION_BUS_ADDRESS").map_err(|e| {
        format!(
            "No DBUS env, did you run systemctl --user import-environment? {:?}",
            e
        )
    })?;

    let opt: Opt = Opt::from_args();
    let config: Config = match serde_json::from_reader(match File::open(&opt.config) {
        Ok(x) => x,
        Err(e) => {
            return Err(format!(
                "Failed to open config file {}: {:?}",
                opt.config.to_string_lossy(),
                e
            ));
        }
    }) {
        Ok(x) => x,
        Err(e) => {
            return Err(format!(
                "Failed to read config file {}: {:?}",
                opt.config.to_string_lossy(),
                e
            ));
        }
    };
    let homeserver_url = &match Url::parse(&config.homeserver) {
        Ok(x) => x,
        Err(e) => {
            return Err(format!(
                "Failed to parse homeserver url {}: {:?}",
                config.homeserver, e
            ));
        }
    };
    let room_id = match RoomId::from_str(&config.room_id) {
        Ok(x) => x,
        Err(e) => {
            return Err(format!(
                "Failed to parse room_id {}: {:?}",
                config.room_id, e
            ));
        }
    };
    let matrix = match Client::new(homeserver_url.clone()) {
        Ok(x) => x,
        Err(e) => {
            return Err(format!("Failed to initialize matrix client: {:?}", e));
        }
    };

    let rt = Builder::new_current_thread().enable_all().build().unwrap();
    LocalSet::new().block_on(&rt, async {
        if let Err(e) = matrix
            .login(&config.name, &config.password, None, None)
            .await
        {
            return Err(format!(
                "Error logging into homeserver {}: {:?}",
                homeserver_url, e
            ));
        }
        'outer: loop {
            let mut dbus_conn = match zbus::azync::Connection::new_session().await {
                Ok(x) => x,
                Err(e) => {
                    eprintln!("Error opening dbus connection: {}", e);
                    sleep(Duration::from_secs(30));
                    continue;
                }
            };
            match dbus_conn
                .call_method(
                    Some("org.freedesktop.DBus"),
                    "/org/freedesktop/DBus",
                    Some("org.freedesktop.DBus.Monitoring"),
                    "BecomeMonitor",
                    &(&[] as &[&str], 0u32),
                )
                .await
            {
                Ok(_) => (),
                Err(e) => {
                    eprintln!("Error enabling monitoring: {}", e);
                    sleep(Duration::from_secs(30));
                    continue;
                }
            };
            while let Some(msg) = match dbus_conn.try_next().await {
                Ok(x) => x,
                Err(e) => {
                    eprintln!("Error fetching next message from connection: {}", e);
                    sleep(Duration::from_secs(30));
                    continue 'outer;
                }
            } {
                let msg_header = match msg.header() {
                    Ok(x) => x,
                    Err(e) => {
                        eprintln!("Failed to read dbus message header {:?}: {:?}", &msg, e);
                        continue;
                    }
                };
                match match &msg_header.message_type() {
                    Ok(x) => x,
                    Err(e) => {
                        eprintln!("Failed to read dbus message type {:?}: {:?}", &msg, e);
                        continue;
                    }
                } {
                    zbus::MessageType::MethodCall => {}
                    _ => continue,
                }
                match match &msg_header.path() {
                    Ok(x) => x,
                    Err(e) => {
                        eprintln!("Failed to read dbus message path {:?}: {:?}", &msg, e);
                        continue;
                    }
                } {
                    Some(p) => {
                        if p.as_str() != "/org/freedesktop/Notifications" {
                            continue;
                        }
                    }
                    None => continue,
                }
                match match &msg_header.member() {
                    Ok(x) => x,
                    Err(e) => {
                        eprintln!("Failed to read dbus message member {:?}: {:?}", &msg, e);
                        continue;
                    }
                } {
                    Some(m) => {
                        if *m != "Notify" {
                            continue;
                        }
                    }
                    None => continue,
                }
                let (
                    app_name,
                    _replaces_id,
                    _app_icon,
                    summary,
                    body,
                    _actions,
                    _hints,
                    _expire_timeout,
                ) = match msg.body::<Notification>() {
                    Ok(x) => x,
                    Err(e) => {
                        eprintln!("Failed to parse dbus notification {:?}: {:?}", &msg, e);
                        continue;
                    }
                };
                let plain_body = &format!("{}: {}\n{}", app_name, summary, body);
                eprintln!("message {}", plain_body);
                let html_body = &format!("<strong>{}</strong>: {}<br>{}", app_name, summary, body);
                match matrix
                    .room_send(
                        &room_id,
                        AnyMessageEventContent::RoomMessage(MessageEventContent::text_html(
                            plain_body, html_body,
                        )),
                        None,
                    )
                    .await
                {
                    Ok(_) => {}
                    Err(e) => {
                        eprintln!("Failed to send message {:?}", e);
                        continue;
                    }
                }
            }
        }
    })?;
    Ok(())
}
